Soporte
Si tiene dudas sobre el API o requiere soporte, 
puede escribirnos un correo electrónico a soporte@smartfact.pe
o nos puede escribirnos por whastapp +51 994186193

Vulnerabilidades de seguridad
Si descubrió una discapacidad de seguridad dentro del API, 
enviamos un correo electrónico soporte@smartfact.pe.
Todas las vulnerabilidades de seguridad serán tratadas con prontitud.

Licencia
El marco del PROYECTO es un software de código abierto con licencia LGPL .